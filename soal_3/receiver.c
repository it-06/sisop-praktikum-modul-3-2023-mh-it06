#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

#define MESSAGE_KEY 12345
#define MAX_MESSAGE_SIZE 100
#define USERS_FILE "users/users.txt"

struct message {
    long mtype;
    char mtext[MAX_MESSAGE_SIZE];
};

struct Credentials {
    char username[50];
    char password[50];
};

void base64_decode(const char *input, char *output) {
    BIO *b64, *bio;
    int input_len = strlen(input);

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    bio = BIO_new(BIO_s_mem());
    BIO_push(b64, bio);

    BIO_write(bio, input, input_len);
    BIO_flush(bio);

    int output_len = BIO_read(b64, output, MAX_MESSAGE_SIZE);

    BIO_free_all(b64);

    output[output_len] = '\0';
}

int processCREDS() {
    FILE *file = fopen(USERS_FILE, "r");
    if (file) {
        char line[256];
        while (fgets(line, sizeof(line), file)) {
            char *username = strtok(line, ":");
            char *encoded_password = strtok(NULL, ":");

            if (encoded_password) {
                char decoded_password[MAX_MESSAGE_SIZE];
                base64_decode(encoded_password, decoded_password);
                printf("Username: %s, Decoded Password: %s\n", username, decoded_password);
            }
        }
        fclose(file);
    } else {
        printf("Failed to open 'users.txt'.\n");
        return 0;
    }
    return 1;
}

int processAUTH(char *username, char *password) {
    FILE *file = fopen(USERS_FILE, "r");
    if (file) {
        char line[256];
        while (fgets(line, sizeof(line), file)) {
            char *fileUsername = strtok(line, ":");
            char *filePassword = strtok(NULL, ":");

            if (fileUsername && filePassword &&
                strcmp(username, fileUsername) == 0 &&
                strcmp(password, filePassword) == 0) {
                fclose(file);
                return 1;
            }
        }
        fclose(file);
    }
    return 0;
}

int processTRANSFER(char *filename) {
    char senderFilePath[100];
    char receiverFilePath[100];

    sprintf(senderFilePath, "Sender/%s", filename);
    sprintf(receiverFilePath, "Receiver/%s", filename);

    FILE *sourceFile = fopen(senderFilePath, "rb");
    if (!sourceFile) {
        printf("File '%s' not found in the Sender directory.\n", filename);
        return 0;
    }

    FILE *destinationFile = fopen(receiverFilePath, "wb");
    if (!destinationFile) {
        printf("Failed to create the file '%s' in the Receiver directory.\n", filename);
        fclose(sourceFile);
        return 0;
    }

    int ch;
    while ((ch = fgetc(sourceFile)) != EOF) {
        fputc(ch, destinationFile);
    }

    fclose(sourceFile);
    fclose(destinationFile);

    printf("File transfer successful: '%s' transferred from Sender to Receiver directory.\n", filename);

    return 1;
}

int main() {
    key_t key = MESSAGE_KEY;
    int msgid;
    struct message msg;

    msgid = msgget(key, 0666 | IPC_CREAT);

    while (1) {
        msgrcv(msgid, &msg, sizeof(msg), 1, 0);

        printf("Received Message: %s\n", msg.mtext);

        if (strcmp(msg.mtext, "CREDS\n") == 0) {
            printf("Received 'CREDS'. Decrypting passwords...\n");
            processCREDS();
        } else if (strncmp(msg.mtext, "AUTH: ", 6) == 0) {
            char *authData = msg.mtext + 6;
            char *authUsername = strtok(authData, " ");
            char *authPassword = strtok(NULL, " ");
            if (authUsername && authPassword) {
                printf("Received 'AUTH' request. Processing...\n");
                if (processAUTH(authUsername, authPassword)) {
                    printf("Authentication successful for user: %s\n", authUsername);
                    strcpy(msg.mtext, "Authentication successful");
                } else {
                    printf("Authentication failed for user: %s\n", authUsername);
                    strcpy(msg.mtext, "Authentication failed");
                    break;
                }
                msg.mtype = 1;
                msgsnd(msgid, &msg, sizeof(msg), 0);
            } else {
                printf("Malformed 'AUTH' request.\n");
            }
        } else if (strncmp(msg.mtext, "TRANSFER ", 8) == 0) {
            char *filename = msg.mtext + 8;
            if (filename) {
                if (processTRANSFER(filename)) {
                    strcpy(msg.mtext, "File transfer successful");
                } else {
                    strcpy(msg.mtext, "File transfer failed");
                    break;
                }
                msg.mtype = 1;
                msgsnd(msgid, &msg, sizeof(msg), 0);
            } else {
                printf("Malformed 'TRANSFER' request.\n");
            }
        } else {
            printf("UNKNOWN COMMAND\n");
        }
    }

    // Receiver akan terus berjalan dalam loop

    return 0;
}

