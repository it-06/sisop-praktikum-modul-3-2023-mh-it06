#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>

// Define untuk key dan pesan maksimum
#define MESSAGE_KEY 12345
#define MAX_MESSAGE_SIZE 100

// Struktur untuk pesan
struct message {
    long mtype;
    char mtext[MAX_MESSAGE_SIZE];
};

int main() {
    key_t key = MESSAGE_KEY;
    int msgid;
    struct message msg;

    // Mendapatkan message queue
    msgid = msgget(key, 0666 | IPC_CREAT);

    while (1) {
        // Menerima input dari pengguna
        printf("Masukkan pesan (CREDS untuk sukses, AUTH: username password, TRANSFER filename): ");
        fgets(msg.mtext, MAX_MESSAGE_SIZE, stdin);

        // Mengisi pesan
        msg.mtype = 1;

        // Mengirim pesan ke message queue
        msgsnd(msgid, &msg, sizeof(msg), 0);

        if (strcmp(msg.mtext, "exit\n") == 0) {
            // Keluar dari loop jika pengguna memasukkan "exit"
            break;
        }

        if (strcmp(msg.mtext, "CREDS\n") == 0) {
            // Setelah mengirim CREDS, berikan prompt lagi
            continue;
        }

        if (strncmp(msg.mtext, "TRANSFER ", 9) == 0) {
            // Kirim perintah "TRANSFER" ke receiver
            continue;
        }

        // Menerima pesan balasan dari receiver
        msgrcv(msgid, &msg, sizeof(msg), 1, 0);

        // Memeriksa apakah pesan balasan adalah pesan keberhasilan
        if (strcmp(msg.mtext, "Authentication successful") == 0) {
            printf("Authentication successful\n");
        } else if (strcmp(msg.mtext, "Authentication failed") == 0) {
            printf("Authentication failed\n");
        } else {
            printf("Pesan dari receiver: %s\n", msg.mtext);
        }
    }

    printf("Sender selesai.\n");

    return 0;
}

