#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

int main() {
    int server_socket, client_socket; //menyimpan deskriptor socket server (server_socket) dan socket klien (client_socket). 
    struct sockaddr_in server_address, client_address; //menggambarkan alamat server (server_address) dan alamat klien (client_address).
    socklen_t client_len = sizeof(client_address); // menyimpan ukuran (panjang) dari struktur alamat klien (client_address).
    char pesan[1024]; // Buffer untuk pesan dari client

   
    server_socket = socket(AF_INET, SOCK_STREAM, 0); // Inisialisasi socket
    if (server_socket == -1) {
        perror("Error in socket");  // Menampilkan pesan kesalahan jika gagal membuat socket
        exit(1);
    }

    
    server_address.sin_family = AF_INET;// Bind socket ke alamat dan port
    server_address.sin_port = htons(12345);// nomor port pada struktur server_address
    server_address.sin_addr.s_addr = INADDR_ANY;//mengatur alamat IP pada struktur server_address ke INADDR_ANY. INADDR_ANY

    if (bind(server_socket, (struct sockaddr*)&server_address, sizeof(server_address)) == -1) {
        perror("Error in bind");  // Menampilkan pesan kesalahan jika gagal melakukan binding
        exit(1);
    }

    
    if (listen(server_socket, 5) == -1) {// Listen hingga 5 koneksi client
        perror("Error in listen");  // Menampilkan pesan kesalahan jika gagal dalam mendengarkan koneksi
        exit(1);
    }

    printf("Server siap menerima koneksi dari 5 client...\n");

   
    int i; // Menerima koneksi dari 5 client
    for (i = 0; i < 5; i++) {
        client_socket = accept(server_socket, (struct sockaddr*)&client_address, &client_len);
        if (client_socket == -1) {
            perror("Error in accept");  // Menampilkan pesan kesalahan jika gagal dalam menerima koneksi
            exit(1);
        }
        printf("Terhubung dengan client %d\n", i + 1);
        
        
        ssize_t bytes_received = recv(client_socket, pesan, sizeof(pesan), 0);// Menerima pesan dari client
        if (bytes_received > 0) {
            pesan[bytes_received] = '\0'; // Menambahkan null-terminator ke pesan yang diterima
            printf("Pesan dari client %d: %s\n", i + 1, pesan);
        }
        
        close(client_socket);  // Menutup koneksi dengan client
    }

    close(server_socket);// Menutup server socket

    return 0;
}