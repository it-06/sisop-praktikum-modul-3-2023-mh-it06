#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/shm.h>
#include <time.h> 

#define ROW1 6
#define COL2 6

long long matrikstranspose[COL2][ROW1];  


long long faktorial(int x) 
{
    if (x <= 1) return 1LL;
    return (long long)x * faktorial(x - 1);
}

int main() 
{
    int shmid;
    key_t key = ftok("belajar.c");
    int (*hasilmatriks)[COL2];

    if ((shmid = shmget(key, sizeof(int[ROW1][COL2]), 0666)) < 0) 
    {
        perror("shmget");
        exit(1);
    }

    if ((hasilmatriks = shmat(shmid, NULL, 0)) == (void*) -1) 
    {
        perror("shmat");
        exit(1);
    }

    waktu_mulai = clock();

  
    for (int i = 0; i < COL2; i++) 
    {
        for (int j = 0; j < ROW1; j++) 
        {
            matrikstranspose[i][j] = hasilmatriks[j][i];
        }
    }

    //Menampilkan matriks transpose
    printf("Hasil transpose matriks:\n");
    for (int i = 0; i < COL2; i++) 
    {
        for (int j = 0; j < ROW1; j++) 
        {
            printf("%lld\t", matrikstranspose[i][j]); 
        }
        printf("\n");
    }

    //Menampilkan hasil faktorial
    printf("Hasil faktorial matriks:\n");
    for (int i = 0; i < COL2; i++) 
    
    {
        for (int j = 0; j < ROW1; j++) 
        {
            long long fakt = faktorial(matrikstranspose[i][j]);
            printf("%lld\t", fakt);  
        }
        printf("\n");
    }


    shmdt(hasilmatriks);

    
    waktu_s end = clock();
    double waktu_cpu = ((double) (end - start));
    printf("waktu eksekusi: %f seconds\n", waktu_cpu);

    return 0;
}


