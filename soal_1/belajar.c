#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROW1 6
#define COL1 2
#define ROW2 2
#define COL2 6

void generateRandomMatriks(int rows, int cols, int matriks[rows][cols], int min, int maks) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            matriks[i][j] = (rand() % (maks - min + 1)) + min;
        }
    }
}

void displayMatriks(int rows, int cols, int matriks[rows][cols]) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%d\t", matriks[i][j]);
        }
        printf("\n");
    }
}

int main() {
    srand(time(NULL));

    int matriks1[ROW1][COL1];
    int matriks2[ROW2][COL2];
    int hasilmatriks[ROW1][COL2];

    generateRandomMatriks(ROW1, COL1, matriks1, 1, 4);
    generateRandomMatriks(ROW2, COL2, matriks2, 1, 5);

    printf("Matriks 1:\n");
    tampilkanmatriks(ROW1, COL1, matriks1);

    printf("\nMatriks 2:\n");
    tampilkanmatriks(ROW2, COL2, matriks2);

    //Fungsi untuk perkalian matriks
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            hasilmatriks[i][j] = 0;
            for (int k = 0; k < COL1; k++) {
                hasilmatriks[i][j] += matriks1[i][k] * matriks2[k][j];
            }
        }
    }

    printf("\nHasil dari Perkalian Matriks:\n");
    tampilkanmatriks(ROW1, COL2, hasilmatriks);

    //Poin b, fungsi untuk mengurangi hasil kali dengan 1
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            hasilmatriks[i][j] -= 1;
        }
    }

    printf("\nHasil dari pengurangan 1:\n");
    tampilkanmatriks(ROW1, COL2, hasilmatriks);

    //Fungsi untuk create shared memory
    key_t key = ftok("belajar.c");
    int shmid = shmget(key, sizeof(hasilmatriks), 0666);

    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }


    int (*shmHasilMatriks)[COL2] = shmat(shmid, NULL, 0);


    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            shmHasilMatriks[i][j] = hasilmatriks[i][j];
        }
    }

    return 0;
}


