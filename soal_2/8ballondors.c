#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>

void downloadFile() {
    system("wget --no-check-certificate 'https://drive.google.com/uc?export=download&id=16FQ11ynf1hSpREPU1jlmzj6xvNl8oosW' -O input.txt");
}

void processFile() {
    FILE *inputFile, *outputFile;
    char c;

    inputFile = fopen("input.txt", "r");
    if (inputFile == NULL) {
        perror("Error opening input file");
        exit(1);
    }

    outputFile = fopen("thebeatles.txt", "w");
    if (outputFile == NULL) {
        perror("Error opening output file");
        fclose(inputFile);
        exit(1);
    }

    while ((c = fgetc(inputFile)) != EOF) {
        if (isalpha(c) || isspace(c)) {
            fputc(c, outputFile);
        }
    }

    fclose(inputFile);
    fclose(outputFile);
}

void countFrequency(char option, int *count, char *search) {
    FILE *file;
    if (option == 'k') {
        file = fopen("thebeatles.txt", "r");
    } else if (option == 'h') {
        file = fopen("thebeatles.txt", "r");
    } else {
        printf("Invalid option.\n");
        return;
    }

    if (file == NULL) {
        perror("Error opening processed file");
        exit(1);
    }

    char buffer[100];
    while (fscanf(file, "%s", buffer) != EOF) {
        if (option == 'k') {
            if (strcmp(buffer, search) == 0) {
                (*count)++;
            }
        } else if (option == 'h') {
            for (int i = 0; buffer[i] != '\0'; i++) {
                if (buffer[i] == search[0]) {
                    (*count)++;
                }
            }
        }
    }

    fclose(file);
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: %s <option>\n", argv[0]);
        printf("Options:\n");
        printf("-kata : Count word frequency\n");
        printf("-huruf : Count character frequency\n");
        return 1;
    }

    int pipe_fd[2];
    if (pipe(pipe_fd) == -1) {
        perror("Error creating pipe");
        exit(1);
    }

    downloadFile(); // Download the file from Google Drive
    processFile();  // Process the downloaded file

    pid_t child_pid = fork();

    if (child_pid == -1) {
        perror("Error forking child process");
        exit(1);
    }

    if (child_pid == 0) {
        // Child process
        close(pipe_fd[0]);

        // Hitung frekuensi kata atau huruf dan kirim hasilnya ke parent process
        int count = 0;
        char search[100];

        if (strcmp(argv[1], "-kata") == 0) {
            printf("Enter the word to search: ");
            scanf("%s", search);
            countFrequency('k', &count, search);
        } else if (strcmp(argv[1], "-huruf") == 0) {
            printf("Enter the character to search: ");
            scanf("%s", search);
            countFrequency('h', &count, search);
        } else {
            printf("Invalid option.\n");
            exit(1);
        }

        if (write(pipe_fd[1], &count, sizeof(count)) == -1) {
            perror("Error writing to pipe");
            exit(1);
        }
        
        if (write(pipe_fd[1], search, sizeof(search)) == -1) {
            perror("Error writing to pipe");
            exit(1);
        }

        close(pipe_fd[1]);
        exit(0);
    } else {
        // Parent process
        close(pipe_fd[1]);

        // Baca hasil perhitungan dari child process
        int count;
        if (read(pipe_fd[0], &count, sizeof(count)) == -1) {
            perror("Error reading from pipe");
            exit(1);
        }
        
        char search[100];
        if (read(pipe_fd[0], search, sizeof(search)) == -1) {
            perror("Error reading from pipe");
            exit(1);
        }

        close(pipe_fd[0]);

        // Pembuatan file log berdasarkan data dari child process
        FILE *logFile = fopen("frekuensi.log", "a");
        if (logFile == NULL) {
            perror("Error opening log file");
            exit(1);
        }

        time_t now;
        time(&now);
        struct tm *tm_info = localtime(&now);
        char timestamp[20];
        strftime(timestamp, 20, "%d/%m/%y %H:%M:%S", tm_info);

        if (strcmp(argv[1], "-kata") == 0) {
            fprintf(logFile, "[%s] [KATA] Kata '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", timestamp, search, count);
            if (count > 0) {
                printf("Kata '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", search, count);
            } else {
                printf("Kata '%s' tidak ditemukan dalam file 'thebeatles.txt'\n", search);
            }
        } else if (strcmp(argv[1], "-huruf") == 0) {
            fprintf(logFile, "[%s] [HURUF] Huruf '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", timestamp, search, count);
            if (count > 0) {
                printf("Huruf '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", search, count);
            } else {
                printf("Huruf '%s' tidak ditemukan dalam file 'thebeatles.txt'\n", search);
            }
        }

        fclose(logFile);
    }

    return 0;
}