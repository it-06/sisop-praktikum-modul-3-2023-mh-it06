# sisop-praktikum-modul 3-2023-MH-IT06

# Anggota

| Nama                   | NRP  |
|------------------------|------------------|
| Mutiara Nurhaliza     | 5027221010       |
| Rafif Dhimaz Ardhana  | 5027221066       |
| Ahmad Fauzan Daniswara | 5027221057      |

## Daftar Isi

- [Soal 1](#soal-1)
  - [Deskripsi Soal 1](#deskripsi-soal-1)
  - [Penyelesaian Soal 1](#penyelesaian-soal-1)
  - [Output Soal 1](#Output-soal-1)
  - [Revisi Soal 1](#Output-soal-1)
  - [Kendala Soal 1](#Output-soal-1)
- [Soal 2](#soal-2)
  - [Deskripsi Soal 2](#deskripsi-soal-2)
  - [Penyelesaian Soal 2](#penyelesaian-soal-2)
  - [Output Soal 2](#Output-soal-2)
  - [Revisi Soal 2](#Output-soal-2)
  - [Kendala Soal 2](#Output-soal-2)
- [Soal 3](#soal-3)
  - [Deskripsi Soal 3](#deskripsi-soal-3)
  - [Penyelesaian Soal 3](#penyelesaian-soal-3)
  - [Output Soal 3](#Output-soal-3)
  - [Revisi Soal 3](#Output-soal-3)
  - [Kendala Soal 3](#Output-soal-3)
- [Soal 4](#soal-4)
  - [Deskripsi Soal 4](#deskripsi-soal-4)
  - [Penyelesaian Soal 4](#penyelesaian-soal-4)
  - [Output Soal 4](#Output-soal-4)
  - [Revisi Soal 4](#Output-soal-4)
  - [Kendala Soal 4](#Output-soal-4)

# Soal 1

## Deskripsi Soal 1

  Pada soal 1 praktikan diminta untuk membuat tiga program dalam bahasa C, yaitu belajar.c, yang.c, rajin.c. 
  Program belajar.c diisi oleh program perkalian matriks dengan ukuran yang bervariasi, yaitu (nomor kelompok)x 2. Selain itu matriks pada program belajar.c berisi angka random dengan rentang pada matriks pertama(1-4) dan rentang pada matriks kedua(1-5). Kemudian, matriks perkalian di program belajar.c dikurangi sebesar 1 pada setiap angka matriksnya.

  Program yang.c diisi dengan konsep shared memory, yang mana program ini akan mengambil variabel hasil pengurangan dari perkalian matriks dari program belajar.c yang kemudian hasil dari pengurangan perkalian matriks tersebut dilakukan transpose matriks dan diperlihatkan hasilnya. Setelah itu, praktikan diminta untuk mencari faktorial pada tiap angka dari hasil trasnpose matriksnya dengan menerapkan thread dan multithreading pada pencarian faktorialnya.

  Program rajin.c berisi hal yang kurang lebih sama dengan apa yang terdapat pada program yang.c. Namun, pada pencarian faktorialnya tidak memakai konsep thread dan multithreading, sehingga praktikan diminta untuk mencari perbedaan output antara program yang.c dan program rajin.c.

## Penyelesaian Soal 1

### Belajar.c 

#### A. Fungsi untuk mendeclare matriks yang akan ditampilkan

```
#define ROW1 6
#define COL1 2
#define ROW2 2
#define COL2 6 //fungsi ini untuk menentukan berapa kolom matriks yg akan tampil

void generateRandomMatriks(int rows, int cols, int matriks[rows][cols], int min, int maks) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            matriks[i][j] = (rand() % (maks - min + 1)) + min;
        }
    }
}

void displayMatriks(int rows, int cols, int matriks[rows][cols]) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%d\t", matriks[i][j]);
        }
        printf("\n");
    }
}

int main() {
    srand(time(NULL));

    int matriks1[ROW1][COL1];
    int matriks2[ROW2][COL2];
    int hasilmatriks[ROW1][COL2];

    generateRandomMatriks(ROW1, COL1, matriks1, 1, 4);
    generateRandomMatriks(ROW2, COL2, matriks2, 1, 5);
```
#### B. Fungsi untuk menampilkan matriks satu dan dua
```
    printf("Matriks 1:\n");
    tampilkanmatriks(ROW1, COL1, matriks1);

    printf("\nMatriks 2:\n");
    tampilkanmatriks(ROW2, COL2, matriks2);
```
#### C. Fungsi untuk melakukan perkalian matriks dan menguranginya dengan 1
```
 for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            hasilmatriks[i][j] = 0;
            for (int k = 0; k < COL1; k++) {
                hasilmatriks[i][j] += matriks1[i][k] * matriks2[k][j];
            }
        }
    }

    printf("\nHasil dari Perkalian Matriks:\n");
    tampilkanmatriks(ROW1, COL2, hasilmatriks); //Fungsi menampilkan perkalian matriks

    //fungsi untuk mengurangi hasil kali dengan 1
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            hasilmatriks[i][j] -= 1;
        }
    }

    printf("\nHasil dari pengurangan 1:\n");
    tampilkanmatriks(ROW1, COL2, hasilmatriks); //Fungsi untuk menampilkan hasil matriks yang telah dikurangi 1

    return 0;
}
```
### Yang.c

#### A. Fungsi untuk mendeclare faktorial yang akan dipakai untuk mengubah matriks 
```
#include <stdio.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#define ROWS1 6
#define COLS2 6

int factorial(int n) {
    if (n <= 1) {
        return 1;
    }
    return n * factorial(n - 1);
}

void *calculateFactorial(void *arg) {
    int (*matrix)[COLS2] = (int (*)[COLS2])arg;

    for (int i = 0; i < COLS2; i++) {
        matrix[i][i] = factorial(matrix[i][i]);
    }
    pthread_exit(NULL);
}
```

#### B. Fungsi untuk menggunakan key memory dari program belajar.c dan program untuk akses shared memory
```
int main() {
    int shmid;
    key_t key = 12345; // Menggunakan key memory dari belajar.c

    // Akses shared memory
    shmid = shmget(key, sizeof(int[ROWS1][COLS2]), 0666 | IPC_CREAT);
    int (*result)[COLS2] = shmat(shmid, (void *)0, 0);

```

#### C. Fungsi untuk transpose matriks dari program belajar.c dan menampilkannya
```
    // Transpose matriks
    int transpose[COLS2][COLS2];
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < COLS2; j++) {
            transpose[i][j] = result[j][i];
        }
    }

    // Tampilkan matriks hasil transpose
    printf("Matriks hasil transpose:\n");
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", transpose[i][j]);
        }
        printf("\n");
    }
```

#### D. Fungsi untuk membuat program faktorial menggunakan thread dan menampilkannya
```

    // Buat thread untuk menghitung faktorial
    pthread_t tid;
    pthread_create(&tid, NULL, calculateFactorial, (void *)transpose);
    pthread_join(tid, NULL);

    // Tampilkan hasil faktorial dalam format matriks
    printf("Matriks hasil faktorial:\n");
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", transpose[i][j]);
        }
        printf("\n");
    }

```

#### E. Fungsi untuk detach shared memory 
```
shmdt(result);
    return 0;
}
```
### Rajin.c

#### A. Fungsi untuk declare faktorial
```
#include <stdio.h>
#include <sys/shm.h>

#define ROWS1 6
#define COLS2 6

int factorial(int n) {
    if (n <= 1) {
        return 1;
    }
    return n * factorial(n - 1);
}
```
#### B. Fungsi untuk menggunakan key memory dari belajar.c dan mengakses shared memory
```
int main() {
    int shmid;
    key_t key = 12345; 

    shmid = shmget(key, sizeof(int[ROWS1][COLS2]), 0666 | IPC_CREAT);
    int (*result)[COLS2] = shmat(shmid, (void *)0, 0);

```
#### C. Fungsi untuk mengtranspose matriks program sebelumnya dan menampilkannya
```
    int transpose[COLS2][COLS2];
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < COLS2; j++) {
            transpose[i][j] = result[j][i];
        }
    }

    // Tampilkan matriks hasil transpose
    printf("Matriks hasil transpose:\n");
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", transpose[i][j]);
        }
        printf("\n");
    }
```
#### D. Fungsi untuk menghitung faktorial matriks dan menampilkannya tanpa memakai thread dan multithreading
```
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < COLS2; j++) {
            transpose[i][j] = factorial(transpose[i][j]);
        }
    }

    printf("Matriks hasil faktorial:\n");
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", transpose[i][j]);
        }
        printf("\n");
    }

```
#### E. Fungsi untuk detach shared memory
```
shmdt(result);
    return 0;
}
```
## Output Soal 1

### Belajar.c
![Output Belajar.c](https://i.imgur.com/FAfEQBb.png)
### Yang.c
![Output Yang.c](https://i.imgur.com/x3tIBUQ.png)
### Rajin.c
![Output Rajin.c](https://i.imgur.com/voBwP7l.png)
Perbedaan antara output yang.c dan rajin.c terdapat pada hasil faktorialnya yg mana program yang.c memakai thread
dan program rajin.c tanpa thread.

## Revisi Soal 1

### Belajar.c
- Menghapus beberapa line code
```
//Fungsi untuk create shared memory
    key_t key = ftok("belajar.c");
    int shmid = shmget(key, sizeof(hasilmatriks), 0666);

    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }


    int (*shmHasilMatriks)[COL2] = shmat(shmid, NULL, 0);


    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            shmHasilMatriks[i][j] = hasilmatriks[i][j];
        }
    }
``` 

### Yang.c
Berikut adalah code revisi yang telah dicoba untuk menjalankan program yang.c
```
#include <stdio.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#define ROWS1 6
#define COLS2 6

int factorial(int n) {
    if (n <= 1) {
        return 1;
    }
    return n * factorial(n - 1);
}

void *calculateFactorial(void *arg) {
    int (*matrix)[COLS2] = (int (*)[COLS2])arg;

    for (int i = 0; i < COLS2; i++) {
        matrix[i][i] = factorial(matrix[i][i]);
    }
    pthread_exit(NULL);
}

int main() {
    int shmid;
    key_t key = 12345; // Menggunakan key memory dari belajar.c

    // Akses shared memory
    shmid = shmget(key, sizeof(int[ROWS1][COLS2]), 0666 | IPC_CREAT);
    int (*result)[COLS2] = shmat(shmid, (void *)0, 0);

    // Transpose matriks
    int transpose[COLS2][COLS2];
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < COLS2; j++) {
            transpose[i][j] = result[j][i];
        }
    }

    // Tampilkan matriks hasil transpose
    printf("Matriks hasil transpose:\n");
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", transpose[i][j]);
        }
        printf("\n");
    }

    // Buat thread untuk menghitung faktorial
    pthread_t tid;
    pthread_create(&tid, NULL, calculateFactorial, (void *)transpose);
    pthread_join(tid, NULL);

    // Tampilkan hasil faktorial dalam format matriks
    printf("Matriks hasil faktorial:\n");
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", transpose[i][j]);
        }
        printf("\n");
    }

    // Detach shared memory
    shmdt(result);
    return 0;
}

```

### Rajin.c
Berikut adalah code revisi yang telah dicoba untuk menjalankan program yang.c
```
#include <stdio.h>
#include <sys/shm.h>

#define ROWS1 6
#define COLS2 6

int factorial(int n) {
    if (n <= 1) {
        return 1;
    }
    return n * factorial(n - 1);
}

int main() {
    int shmid;
    key_t key = 12345; // Menggunakan key memory dari belajar.c

    // Akses shared memory
    shmid = shmget(key, sizeof(int[ROWS1][COLS2]), 0666 | IPC_CREAT);
    int (*result)[COLS2] = shmat(shmid, (void *)0, 0);

    // Transpose matriks
    int transpose[COLS2][COLS2];
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < COLS2; j++) {
            transpose[i][j] = result[j][i];
        }
    }

    // Tampilkan matriks hasil transpose
    printf("Matriks hasil transpose:\n");
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", transpose[i][j]);
        }
        printf("\n");
    }

    // Hitung faktorial dan tampilkan hasil dalam format matriks
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < COLS2; j++) {
            transpose[i][j] = factorial(transpose[i][j]);
        }
    }

    printf("Matriks hasil faktorial:\n");
    for (int i = 0; i < COLS2; i++) {
        for (int j = 0; j < COLS2; j++) {
            printf("%d ", transpose[i][j]);
        }
        printf("\n");
    }

    // Detach shared memory
    shmdt(result);
    return 0;
}

```
## Kendala Soal 1

### Belajar.c
Tidak ada

### Yang.c
Kendalanya adalah mencoba untuk menemukan fungsi dan code yg benar untuk menjalankan program rajin.c, namun outputnya masih jauh dari contoh.

### Rajin.c
Kendalanya adalah mencoba untuk menemukan fungsi dan code yg benar untuk menjalankan program rajin.c, namun outputnya masih jauh dari contoh.

# Soal 2

## Deskripsi Soal 
Messi ingin membuat program bernama "8ballondors.c" yang dapat melakukan beberapa tugas terkait pengolahan teks pada lagu favoritnya dan menghitung frekuensi kemunculan kata atau huruf. Program ini menggunakan konsep pipes dan fork, dengan tugas yang dibagi antara parent process dan child process.

Parent Process:
Membaca sebuah file yang berisi lirik lagu favorit Messi.
Menghapus karakter-karakter yang bukan huruf dari file tersebut.
Menyimpan hasilnya dalam file baru dengan nama "thebeatles.txt".
Menerima input dari user untuk menentukan apakah akan menghitung frekuensi kemunculan kata atau huruf.
Child Process:
Menghitung frekuensi kemunculan kata atau huruf dari file "thebeatles.txt" berdasarkan input pengguna.
Mengirim hasil perhitungan frekuensi kemunculan kata atau huruf ke parent process.
Argumen Program:
Pengguna menjalankan program dengan argumen "-kata" atau "-huruf" untuk menentukan apakah program harus menghitung frekuensi kata atau huruf.
Logging:
Setiap kata atau huruf yang dihitung akan dicatat dalam sebuah file log dengan nama "frekuensi.log".
Log akan berisi tanggal, jenis perhitungan (KATA atau HURUF), dan pesan yang menjelaskan hasil perhitungan.
Contoh Format Log:

[24/10/23 01:05:48] [KATA] Kata 'yang' muncul sebanyak 10 kali dalam file 'thebeatles.txt'
[24/10/23 01:04:29] [HURUF] Huruf 'a' muncul sebanyak 396 kali dalam file 'thebeatles.txt'
Perhitungan frekuensi kemunculan kata atau huruf harus menggunakan Case-Sensitive Search, artinya program harus mempertimbangkan huruf besar dan huruf kecil sebagai karakter yang berbeda. 
## Penyelesaian Soal

### Point A

Pada parent process, program akan membaca file dan menghapus karakter-karakter yang bukan huruf pada file dan membuat output file dari hasil proses itu dengan nama thebeatles.txt

```
void processFile() {
    FILE *inputFile, *outputFile; //deklarasi dua pointer ke objek FILE, yaitu 
    char c; //membaca karakter dari file input.

    inputFile = fopen("input.txt", "r"); //membuka file "input.txt" dalam mode baca ("r") dan menugaskan pointer FILE ke variabel inputFile.
    if (inputFile == NULL) { // penanganan jika input file tidak dapat dibuka
        perror("Error opening input file");
        exit(1);
    }

    outputFile = fopen("thebeatles.txt", "w");//membuka file "thebeatles.txt" dalam mode tulis ("w") dan menugaskan pointer FILE ke variabel outputFile.
    if (outputFile == NULL) { // penanganan jika gagal membuka output file
        perror("Error opening output file");
        fclose(inputFile);
        exit(1);
    }

    while ((c = fgetc(inputFile)) != EOF) { // while berjalan sampai akhir file (EOF).
        if (isalpha(c) || isspace(c)) { //karakter dibaca menggunakan fgetc dari inputFile,  diperiksa apakah karakter adalah huruf atau karakter spasi menggunakan fungsi isalpha dan isspace.
            fputc(c, outputFile); //jika karakter adalah huruf atau spasi, maka karakter tersebut dituliskan ke outputFile menggunakan fputc.
        }
    }

    fclose(inputFile); // menutup file input
    fclose(outputFile); // menutup file output
}

```

Kode `processFile()` adalah sebuah fungsi dalam program "8ballondors.c" yang memiliki tanggung jawab untuk membersihkan file lirik lagu yang ada dalam file "input.txt" dari karakter yang bukan huruf, dan hasilnya disimpan dalam file "thebeatles.txt". Berikut adalah penjelasan rinci dari kode ini:

1. `FILE *inputFile, *outputFile;`: Ini adalah deklarasi dua pointer ke objek FILE yang akan digunakan untuk membuka dan mengakses file. `inputFile` digunakan untuk membaca isi dari file lirik lagu "input.txt", dan `outputFile` digunakan untuk menulis hasil pemrosesan ke file "thebeatles.txt".

2. `inputFile = fopen("input.txt", "r");`: Fungsi `fopen()` digunakan untuk membuka file "input.txt" dalam mode "r" (baca). Hasilnya akan ditugaskan ke pointer `inputFile`. Jika file tidak dapat dibuka, maka pesan kesalahan akan dicetak menggunakan `perror()` dan program akan keluar dengan kode kesalahan (exit(1)).

3. `outputFile = fopen("thebeatles.txt", "w");`: Fungsi `fopen()` digunakan untuk membuka file "thebeatles.txt" dalam mode "w" (tulis). Hasilnya akan ditugaskan ke pointer `outputFile`. Jika file tidak dapat dibuka, pesan kesalahan akan dicetak, file input ditutup, dan program akan keluar dengan kode kesalahan.

4. Kemudian, program akan memproses file lirik lagu karakter per karakter. Ini dilakukan dalam loop `while` yang berjalan selama belum mencapai akhir file (EOF).

5. Di dalam loop, karakter dari file "inputFile" dibaca menggunakan `fgetc()` dan disimpan dalam variabel `c`.

6. Setiap karakter yang dibaca diuji menggunakan dua kondisi: `isalpha(c)` dan `isspace(c)`. 
   - `isalpha(c)` digunakan untuk memeriksa apakah karakter adalah huruf.
   - `isspace(c)` digunakan untuk memeriksa apakah karakter adalah spasi (whitespace).
   - Jika karakter adalah huruf atau spasi, maka karakter tersebut dianggap valid dan akan ditulis ke file "thebeatles.txt" menggunakan `fputc(c, outputFile)`. Ini bertujuan untuk menghapus karakter-karakter yang bukan huruf dari lirik lagu.

7. Setelah selesai membaca dan memproses seluruh isi file "input.txt", kedua file, yaitu "inputFile" dan "outputFile", ditutup menggunakan `fclose()` untuk memastikan semua perubahan ditulis ke file "thebeatles.txt".

Kode ini akan membersihkan file lirik lagu dari karakter yang bukan huruf dan menyimpan hasilnya dalam file "thebeatles.txt", yang akan digunakan untuk perhitungan selanjutnya dalam program.
### Point B, C, dan D

Pada child process, melakukan perhitungan jumlah frekuensi kemunculan sebuah kata dan frekuensi kemunculan sebuah huruf dari output file yang akan diinputkan user. Membuat argumen untuk menjalankan program

Kata	: ./8ballondors -kata
Huruf	: ./8ballondors -huruf



```
void countFrequency(char option, int *count, char *search) { //  option, yang merupakan karakter 'k' atau 'h', count, yang merupakan pointer ke integer, dan search, yang merupakan pointer ke karakter.
    FILE *file;
    //membuka file "thebeatles.txt" dalam mode baca ("r").
    //Opsi 'k' dan 'h' digunakan untuk menentukan cara penghitungan
    if (option == 'k') { 
        file = fopen("thebeatles.txt", "r");
    } else if (option == 'h') {
        file = fopen("thebeatles.txt", "r");
    } else {
        printf("Invalid option.\n");
        return;
    }

    if (file == NULL) { // pesan kesalahan jika file tidak dapat dibuka
        perror("Error opening processed file");
        exit(1);
    }

    char buffer[100]; //menyimpan kata-kata dari file yang dibaca.
    while (fscanf(file, "%s", buffer) != EOF) { // membaca file sampai akhir file (EOF)
        if (option == 'k') {// jika opsi k
            if (strcmp(buffer, search) == 0) { //memeriksa apakah kata yang dibaca (disimpan di dalam buffer) sama dengan kata yang dicari (yang disimpan di dalam search) menggunakan strcmp
                (*count)++; //Jika kedua kata tersebut sama, maka variabel yang ditunjuk oleh count (yaitu jumlah kemunculan) akan ditambah satu.
            }
        } else if (option == 'h') { // jika opsi h
            for (int i = 0; buffer[i] != '\0'; i++) { //melakukan perulangan untuk setiap karakter di dalam buffer  
                if (buffer[i] == search[0]) {//memeriksa apakah karakter tersebut sama dengan karakter pertama dari search. 
                    (*count)++; //Jika sama, maka jumlah kemunculan akan ditambah satu.
                }
            }
        }
    }

    fclose(file); // menutup file output
}

```

1. **Parameter Fungsi**:
   - `option`: Parameter ini adalah karakter yang menentukan jenis perhitungan yang akan dilakukan. 'k' digunakan untuk menghitung frekuensi kemunculan kata, dan 'h' digunakan untuk menghitung frekuensi kemunculan huruf.
   - `count`: Parameter ini adalah pointer ke integer yang digunakan untuk menyimpan jumlah kemunculan kata atau huruf yang dicari.
   - `search`: Parameter ini adalah pointer ke karakter yang berisi kata atau huruf yang akan dicari.

2. **Membuka File "thebeatles.txt"**:
   - Fungsi `fopen()` digunakan untuk membuka file "thebeatles.txt" dalam mode "r" (baca). File ini akan digunakan untuk mencari frekuensi kemunculan kata atau huruf.

3. **Pemeriksaan Opsi**:
   - Pada blok `if (option == 'k')`, program akan memeriksa apakah opsi adalah 'k' (menghitung kata). Jika iya, maka program akan membuka file "thebeatles.txt" dalam mode baca. Blok `else if (option == 'h')` mirip dengan blok 'k', tetapi ini digunakan untuk menghitung frekuensi kemunculan huruf. Jika opsi tidak valid, maka akan dicetak pesan kesalahan ("Invalid option.") dan fungsi akan segera keluar.

4. **Pengolahan Kata atau Huruf**:
   - Dalam loop `while`, program membaca file sampai mencapai akhir file (EOF).
   - Fungsi `fscanf()` digunakan untuk membaca kata (atau huruf) dari file "thebeatles.txt" dan menyimpannya dalam variabel `buffer`.
   - Dalam blok `if (option == 'k')`, program memeriksa apakah opsi adalah 'k' (menghitung kata). Jika ya, maka program membandingkan kata yang dibaca (`buffer`) dengan kata yang dicari (`search`) menggunakan `strcmp()`. Jika kedua kata tersebut sama, maka variabel yang ditunjuk oleh `count` (yaitu jumlah kemunculan) akan ditambah satu.
   - Dalam blok `else if (option == 'h')`, program memeriksa apakah opsi adalah 'h' (menghitung huruf). Jika ya, program melakukan perulangan untuk setiap karakter di dalam `buffer`. Program memeriksa apakah karakter tersebut sama dengan karakter pertama dari `search`. Jika sama, jumlah kemunculan akan ditambah satu.

5. **Penutupan File**:
   - Setelah selesai menghitung frekuensi, file "thebeatles.txt" ditutup menggunakan `fclose()` untuk memastikan semua perubahan ditulis ke file.

### Point E

Hasil dari perhitungan  jumlah frekuensi kemunculan sebuah kata dan  jumlah frekuensi kemunculan sebuah huruf dikirim ke parent process.

```
int main(int argc, char *argv[]) {
    if (argc != 2) {// pesan kesalahan jika input salah
        printf("Usage: %s <option>\n", argv[0]);
        printf("Options:\n");
        printf("-kata : Count word frequency\n");
        printf("-huruf : Count character frequency\n");
        return 1;
    }

    int pipe_fd[2]; // pipe untuk mengirim hasil perhitungan dari proses anak ke proses induk.
    if (pipe(pipe_fd) == -1) {
        perror("Error creating pipe");
        exit(1);
    }

    downloadFile(); // Download the file from Google Drive
    processFile();  // Process the downloaded file

    pid_t child_pid = fork(); // fork untuk membuat proses anak. Proses anak melakukan perhitungan frekuensi kata atau karakter dan mengirim hasilnya ke proses induk.

    if (child_pid == -1) {
        perror("Error forking child process");
        exit(1);
    }

    if (child_pid == 0) {
        // Child process
        close(pipe_fd[0]);//menutup (pipe_fd[0]) proses anak. karena proses anak akan menulis ke pipa

        // Hitung frekuensi kata atau huruf dan kirim hasilnya ke parent process
        int count = 0;// menyimpan jumlah kemunculan kata atau huruf
        char search[100]; // menyimpan kata atau huruf yang dicari

        if (strcmp(argv[1], "-kata") == 0) {// memeriksa apakah opsi nya -kata
            printf("Enter the word to search: ");
            scanf("%s", search);//membaca input dan menyimpan dalam variabel search
            countFrequency('k', &count, search); //memanggil fungsi countFrequency dengan argumen 'k' untuk menghitung frekuensi kata dalam file. Hasil perhitungan disimpan dalam variabel count.
        } else if (strcmp(argv[1], "-huruf") == 0) {// memeriksa apakah opsi nya -huruf
            printf("Enter the character to search: ");
            scanf("%s", search); //membaca input dan menyimpan dalam variabel search
            countFrequency('h', &count, search);// memanggil fungsi countFrequency dengan argumen 'h' untuk menghitung frekuensi karakter dalam file. Hasil perhitungan disimpan dalam variabel count.
        } else {
            printf("Invalid option.\n"); // penanganan kesalahan jika opsi tidak valid
            exit(1);
        }
        //menulis hasil perhitungan (variabel count) ke ujung penulisan pipa (pipe_fd[1]) agar proses induk dapat membacanya.
        if (write(pipe_fd[1], &count, sizeof(count)) == -1) {
            perror("Error writing to pipe");
            exit(1);
        }
        
        //menulis hasil perhitungan (variabel search) ke ujung penulisan pipa (pipe_fd[1]) agar proses induk dapat membacanya.
        if (write(pipe_fd[1], search, sizeof(search)) == -1) {
            perror("Error writing to pipe");
            exit(1);
        }

        //proses anak menutup pipe
        close(pipe_fd[1]);
        exit(0);
    } else {
        // Parent process
        close(pipe_fd[1]);//mencegah deadlock

        // Baca hasil perhitungan dari child process
        int count;
        if (read(pipe_fd[0], &count, sizeof(count)) == -1) {//membaca nilai count yang dikirim oleh proses anak melalui pipa. 
            perror("Error reading from pipe");
            exit(1);
        }
        
        char search[100];
        if (read(pipe_fd[0], search, sizeof(search)) == -1) {//membaca kata atau karakter yang dicari (search) yang dikirim oleh proses anak melalui pipa.
            perror("Error reading from pipe");
            exit(1);
        }

        close(pipe_fd[0]);//membersihkan sumber daya yang digunakan oleh pipa.

        // Pembuatan file log berdasarkan data dari child process
        FILE *logFile = fopen("frekuensi.log", "a");//membuat file "frekuensi.log" dalam mode "a" (append) untuk menambahkan catatan log.
        if (logFile == NULL) {//penanganan file tidak dapat dibuka
            perror("Error opening log file");
            exit(1);
        }

        //mengambil waktu saat ini dan membentuk timestamp yang akan digunakan dalam catatan log. 
        time_t now;
        time(&now);
        struct tm *tm_info = localtime(&now);
        char timestamp[20];
        strftime(timestamp, 20, "%d/%m/%y %H:%M:%S", tm_info);

        if (strcmp(argv[1], "-kata") == 0) {//Jika opsi pada baris perintah (argv[1]) adalah "-kata," program mencetak catatan log yang mencakup timestamp, kata yang dicari, dan jumlah kemunculan kata tersebut dalam file "thebeatles.txt".
            fprintf(logFile, "[%s] [KATA] Kata '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", timestamp, search, count);
            if (count > 0) {
                printf("Kata '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", search, count);
            } else {
                printf("Kata '%s' tidak ditemukan dalam file 'thebeatles.txt'\n", search);
            }
        } else if (strcmp(argv[1], "-huruf") == 0) {//Jika opsi pada baris perintah (argv[1]) adalah "-huruf," program mencetak catatan log yang mencakup timestamp, karakter yang dicari, dan jumlah kemunculan karakter tersebut dalam file "thebeatles.txt". 
            fprintf(logFile, "[%s] [HURUF] Huruf '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", timestamp, search, count);
            if (count > 0) {
                printf("Huruf '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", search, count);
            } else {
                printf("Huruf '%s' tidak ditemukan dalam file 'thebeatles.txt'\n", search);
            }
        }

        fclose(logFile);//menutup file log
    }

    return 0;
}
```
Kode yang Anda tunjukkan adalah program dalam bahasa C yang mengambil opsi dari argumen baris perintah (command line) dan melakukan beberapa tugas berikut:

1. **Validasi Argumen**: Program melakukan validasi untuk memastikan bahwa hanya satu argumen yang dibutuhkan dan opsi yang valid, yaitu `-kata` untuk menghitung frekuensi kata atau `-huruf` untuk menghitung frekuensi huruf. Jika argumen tidak sesuai, program mencetak pesan kesalahan dan keluar.

2. **Pipe Creation**: Program membuat sebuah pipa (`pipe_fd`) menggunakan sistem panggilan `pipe`. Pipa ini akan digunakan untuk mengirim hasil perhitungan dari proses anak ke proses induk.

3. **Download dan Pemrosesan File**: Program memanggil dua fungsi: `downloadFile` untuk mengunduh file dari Google Drive dan menyimpannya sebagai "input.txt", dan `processFile` untuk membersihkan file "input.txt" dan menyimpan hasilnya dalam file "thebeatles.txt".

4. **Pembuatan Proses Anak**: Program menggunakan sistem panggilan `fork` untuk membuat proses anak. Proses anak melakukan perhitungan frekuensi kata atau huruf dan mengirim hasilnya ke proses induk melalui pipa.

5. **Proses Anak**: Dalam proses anak (`if (child_pid == 0)`), program melakukan hal berikut:
   - Menutup ujung pembacaan pipa (`pipe_fd[0]`) karena proses anak hanya akan menulis ke pipa.
   - Meminta pengguna untuk memasukkan kata atau karakter yang akan dicari, kemudian menyimpannya dalam variabel `search`.
   - Memanggil fungsi `countFrequency` dengan opsi yang sesuai (`'k'` atau `'h'`) untuk menghitung frekuensi kata atau huruf dalam file "thebeatles.txt". Hasil perhitungan disimpan dalam variabel `count`.
   - Mengirim hasil perhitungan (`count` dan `search`) ke proses induk melalui pipa menggunakan sistem panggilan `write`.
   - Menutup ujung penulisan pipa (`pipe_fd[1]`) setelah selesai mengirim data.
   - Proses anak keluar dengan `exit(0)`.

6. **Proses Induk**: Dalam proses induk (`else`), program melakukan hal berikut:
   - Menutup ujung penulisan pipa (`pipe_fd[1]`) untuk mencegah deadlock.
   - Membaca hasil perhitungan dari proses anak yang dikirim melalui pipa menggunakan sistem panggilan `read` dan menyimpannya dalam variabel `count` dan `search`.
   - Membuka atau membuat file log ("frekuensi.log") dalam mode "a" (append) untuk menambahkan catatan log.
   - Mengambil timestamp saat ini dan membentuk string timestamp yang akan digunakan dalam catatan log.
   - Berdasarkan opsi yang dipilih pada baris perintah (`argv[1]`), program mencetak catatan log yang mencakup timestamp, kata atau karakter yang dicari, dan jumlah kemunculan kata atau karakter tersebut dalam file "thebeatles.txt".
   - Program juga mencetak pesan ke layar sesuai dengan hasil perhitungan.
   - Program menutup file log setelah menulis log selesai.

Program ini secara rinci menjelaskan bagaimana hasil perhitungan jumlah frekuensi kemunculan sebuah kata atau karakter dikirim ke proses induk, dan juga bagaimana catatan log dibuat berdasarkan hasil perhitungan ini.

### Point F

Setiap kata atau huruf dicatat dalam sebuah log yang diberi nama frekuensi.log. Pada parent process, lakukan pembuatan file log berdasarkan data yang dikirim dari child process. 
Format: [date] [type] [message]
Type: KATA, HURUF
Ex:
[24/10/23 01:05:48] [KATA] Kata 'yang' muncul sebanyak 10 kali dalam file 'thebeatles.txt'
[24/10/23 01:04:29] [HURUF] Huruf 'a' muncul sebanyak 396 kali dalam file 'thebeatles.txt'


```
 time_t now;
        time(&now);
        struct tm *tm_info = localtime(&now);
        char timestamp[20];
        strftime(timestamp, 20, "%d/%m/%y %H:%M:%S", tm_info);

        if (strcmp(argv[1], "-kata") == 0) {//Jika opsi pada baris perintah (argv[1]) adalah "-kata," program mencetak catatan log yang mencakup timestamp, kata yang dicari, dan jumlah kemunculan kata tersebut dalam file "thebeatles.txt".
            fprintf(logFile, "[%s] [KATA] Kata '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", timestamp, search, count);
            if (count > 0) {
                printf("Kata '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", search, count);
            } else {
                printf("Kata '%s' tidak ditemukan dalam file 'thebeatles.txt'\n", search);
            }
        } else if (strcmp(argv[1], "-huruf") == 0) {//Jika opsi pada baris perintah (argv[1]) adalah "-huruf," program mencetak catatan log yang mencakup timestamp, karakter yang dicari, dan jumlah kemunculan karakter tersebut dalam file "thebeatles.txt". 
            fprintf(logFile, "[%s] [HURUF] Huruf '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", timestamp, search, count);
            if (count > 0) {
                printf("Huruf '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", search, count);
            } else {
                printf("Huruf '%s' tidak ditemukan dalam file 'thebeatles.txt'\n", search);
            }
        }

        fclose(logFile);//menutup file log

```

1. **Mengambil Timestamp**:
   - `time_t now;`: Variabel `now` bertipe `time_t` digunakan untuk menyimpan waktu saat ini.
   - `time(&now);`: Fungsi `time(&now)` mengisi nilai `now` dengan waktu saat ini, yaitu jumlah detik sejak "epoch" (biasanya 1 Januari 1970).

2. **Mengonversi Timestamp ke Format Tertentu**:
   - `struct tm *tm_info = localtime(&now);`: Fungsi `localtime` digunakan untuk mengonversi waktu dalam `time_t` ke struktur `tm` yang berisi informasi tanggal dan waktu terperinci.
   - `char timestamp[20];`: Variabel `timestamp` bertipe `char` digunakan untuk menyimpan timestamp yang akan digunakan dalam catatan log.
   - `strftime(timestamp, 20, "%d/%m/%y %H:%M:%S", tm_info);`: Fungsi `strftime` digunakan untuk memformat waktu ke dalam string berdasarkan format yang ditentukan. Dalam contoh ini, timestamp akan berformat seperti "dd/mm/yy HH:MM:SS".

3. **Membuat Catatan Log**:
   - Pengecekan argumen: Program memeriksa argumen yang diberikan pada baris perintah (`argv[1]`) untuk menentukan apakah pengguna meminta perhitungan kata (`-kata`) atau karakter (`-huruf`).

4. **Mencetak ke File Log**:
   - Jika opsi adalah `-kata`, program menggunakan `fprintf` untuk mencetak catatan log yang mencakup timestamp, kata yang dicari (`search`), dan jumlah kemunculan kata tersebut dalam file "thebeatles.txt". Misalnya:
     ```
     [timestamp] [KATA] Kata 'search' muncul sebanyak count kali dalam file 'thebeatles.txt'
     ```
   - Jika opsi adalah `-huruf`, program mencetak catatan log yang mencakup timestamp, karakter yang dicari, dan jumlah kemunculan karakter tersebut dalam file "thebeatles.txt". Misalnya:
     ```
     [timestamp] [HURUF] Huruf 'search' muncul sebanyak count kali dalam file 'thebeatles.txt'
     ```

5. **Mencetak Pesan ke Layar**:
   - Program juga mencetak pesan ke layar sesuai dengan hasil perhitungan. Jika jumlah kemunculan (`count`) lebih besar dari 0, program mencetak berapa kali kata atau karakter ditemukan dalam file. Jika tidak ada yang ditemukan, program memberi pesan bahwa kata atau karakter tidak ditemukan.

6. **Menutup File Log**:
   - Setelah menulis catatan log dan mencetak pesan, program menggunakan `fclose(logFile)` untuk menutup file log yang telah dibuka sebelumnya.

Jadi, potongan kode ini digunakan untuk membuat catatan log yang mencakup hasil perhitungan frekuensi kata atau karakter, dengan menambahkan timestamp yang menunjukkan waktu ketika perhitungan tersebut dilakukan. Ini berguna untuk mencatat dan melacak hasil perhitungan yang telah dilakukan.

## Output

**Menjalankan dengan argumen kata**
![Membuat File](https://i.imgur.com/SNUn1cY.png)

**Menjalankan dengan argumen huruf**
![Mengunduh Gambar](https://i.imgur.com/HzGkr3L.png)

**Hasil log.txt**
![Hasil log.txt](https://i.imgur.com/59mKl6o.png)

## Revisi

Tidak ada

## Kendala

Tidak ada

# Soal 3

## Deskripsi Soal 3
Membuat program dengan konsep message queue yang akan menerima pesan dari `sender.c` ke `receiver.c`. Pesan akan dicek dan dicompare pada fungsi yang telah dideklarasikan pada file `receiver.c` dan akan berjalan sesuai input pesan dari `sender.c`. 

+ Apabila pesan yang diinput pada `sender.c` adalah CREDS, maka file `receiver.c` akan memberikan output list username beserta password yang telah didekripsi.

+ Apabila pesan yang diinput pada `sender.c` adalah AUTH: _username password_ maka program `receiver.c` akan melakukan proses autentikasi dengan username dan password yang telah didekripsi tersebut.

+ Apabila pesan yang diinput pada `sender.c` adalah TRANSFER _filename_ maka program `receiver.c` akan melakukan proses transfer file dari direktori Sender ke direktori Receiver.

## Penyelesaian Soal 3
`sender.c`
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define MESSAGE_KEY 12345
#define MAX_MESSAGE_SIZE 100

struct message {
    long mtype;
    char mtext[MAX_MESSAGE_SIZE];
};

int main() {
    key_t key = MESSAGE_KEY;
    int msgid;
    struct message msg;

    msgid = msgget(key, 0666 | IPC_CREAT);

    while (1) {
        printf("Masukkan pesan (CREDS untuk sukses, AUTH: username password, TRANSFER filename): ");
        fgets(msg.mtext, MAX_MESSAGE_SIZE, stdin);

        msg.mtype = 1;

        msgsnd(msgid, &msg, sizeof(msg), 0);

        if (strcmp(msg.mtext, "exit\n") == 0) {
            // Keluar dari loop jika pengguna memasukkan "exit"
            break;
        }

        if (strcmp(msg.mtext, "CREDS\n") == 0) {
            // Setelah mengirim CREDS, berikan prompt lagi
            continue;
        }

        if (strncmp(msg.mtext, "TRANSFER ", 9) == 0) {
            // Kirim perintah "TRANSFER" ke receiver
            continue;
        }

        msgrcv(msgid, &msg, sizeof(msg), 1, 0);

        if (strcmp(msg.mtext, "Authentication successful") == 0) {
            printf("Authentication successful\n");
        } else if (strcmp(msg.mtext, "Authentication failed") == 0) {
            printf("Authentication failed\n");
        } else {
            printf("Pesan dari receiver: %s\n", msg.mtext);
        }
    }

    printf("Sender selesai.\n");

    return 0;
}

```

`receiver.c`
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

#define MESSAGE_KEY 12345
#define MAX_MESSAGE_SIZE 100
#define USERS_FILE "users/users.txt"

struct message {
    long mtype;
    char mtext[MAX_MESSAGE_SIZE];
};

void base64_decode(const char *input, char *output) {
    BIO *b64, *bio;
    int input_len = strlen(input);

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    bio = BIO_new(BIO_s_mem());
    BIO_push(b64, bio);

    BIO_write(bio, input, input_len);
    BIO_flush(bio);

    int output_len = BIO_read(b64, output, MAX_MESSAGE_SIZE);

    BIO_free_all(b64);

    output[output_len] = '\0';
}

int processCREDS() {
    FILE *file = fopen(USERS_FILE, "r");
    if (file) {
        char line[256];
        while (fgets(line, sizeof(line), file)) {
            char *username = strtok(line, ":");
            char *encoded_password = strtok(NULL, ":");

            if (encoded_password) {
                char decoded_password[MAX_MESSAGE_SIZE];
                base64_decode(encoded_password, decoded_password);
                printf("Username: %s, Decoded Password: %s\n", username, decoded_password);
            }
        }
        fclose(file);
    } else {
        printf("Failed to open 'users.txt'.\n");
        return 0;
    }
    return 1;
}

int processAUTH(char *username, char *input_password) {
    FILE *file = fopen(USERS_FILE, "r");
    if (file) {
        char line[256];
        while (fgets(line, sizeof(line), file)) {
            char *fileUsername = strtok(line, ":");
            char *encoded_password = strtok(NULL, ":");

            if (fileUsername && encoded_password) {
                char decoded_password[MAX_MESSAGE_SIZE];
                base64_decode(encoded_password, decoded_password);

                if (strcmp(username, fileUsername) == 0 &&
                    strcmp(input_password, decoded_password) == 0) {
                    fclose(file);
                    return 1;
                }
            }
        }
        fclose(file);
    }
    return 0;
}


int processTRANSFER(char *filename) {
    char source[300];
    char destination[300];

    sprintf(source, "Sender/%s", senderFile);
    sprintf(destination, "Receiver/%s", senderFile);

    struct stat st;
    if (stat(destination, &st) == 0){
        printf("Found a duplicate file in 'Receiver' directory\n");
    }
    else{
        if (rename(source, destination) == 0){
            if (stat(destination, &st) == 0){
                printf("Ukuran file yang dipindah: %lld KB\n", (long long)st.st_size / 1024);}
            else{
                perror("Error mendapatkan ukuran file");
            }
        }
    }
}

int main() {
    key_t key = MESSAGE_KEY;
    int msgid;
    struct message msg;
    int authenticated = 0;

    msgid = msgget(key, 0666 | IPC_CREAT);

    while (1) {
        msgrcv(msgid, &msg, sizeof(msg), 1, 0);

        printf("Received Message: %s\n", msg.mtext);

        if (strcmp(msg.mtext, "CREDS\n") == 0) {
            printf("Received 'CREDS'. Decrypting passwords...\n");
            processCREDS();
        } else if (strncmp(msg.mtext, "AUTH: ", 6) == 0) {
            char *authData = msg.mtext + 6;
            char *authUsername = strtok(authData, " ");
            char *authPassword = strtok(NULL, " ");
            if (authUsername && authPassword) {
                printf("Received 'AUTH' request. Processing...\n");
                if (processAUTH(authUsername, authPassword)) {
                    printf("Authentication successful for user: %s\n", authUsername);
                    strcpy(msg.mtext, "Authentication successful");
                    authenticated = 1;
                } else {
                    printf("Authentication failed for user: %s\n", authUsername);
                    strcpy(msg.mtext, "Authentication failed");
                    break;
                }
                msg.mtype = 1;
                msgsnd(msgid, &msg, sizeof(msg), 0);
            } else {
                printf("Malformed 'AUTH' request.\n");
            }
        } else if (strncmp(msg.mtext, "TRANSFER ", 9) == 0) {
            char *filename = msg.mtext + 9;
            if(authenticated == 1){
                if (filename) {
                    if (processTRANSFER(filename)) {
                        strcpy(msg.mtext, "File transfer successful");
                    } else {
                        strcpy(msg.mtext, "File transfer failed");
                        break;
                    }
                    msg.mtype = 1;
                    msgsnd(msgid, &msg, sizeof(msg), 0);
                } else {
                    printf("Malformed 'TRANSFER' request.\n");
                }
            }else{
                printf("User not Authenticated\n");
            }
        } else {
            printf("UNKNOWN COMMAND\n");
        }
    }

    // Receiver akan terus berjalan dalam loop

    return 0;
}


```
### Penjelasan Sender
`sender.c`
```
// Struktur untuk pesan
struct message {
    long mtype;
    char mtext[MAX_MESSAGE_SIZE];
};
```

Disini dibuat fungsi struct untuk melakukan kegiatan message queue dengan variabel yang telah dideclare pada DEFINE dibagian atas program.

```
int main() {
    key_t key = MESSAGE_KEY;
    int msgid;
    struct message msg;

    // Mendapatkan message queue
    msgid = msgget(key, 0666 | IPC_CREAT);

```

Bagian main ini digunakan untuk mendeklarasikan konsep message queue pada program yang mana pada bagian ini dibuatnya unique key.

```
while (1) {
        // Menerima input dari pengguna
        printf("Masukkan pesan (CREDS untuk sukses, AUTH: username password, TRANSFER filename): ");
        fgets(msg.mtext, MAX_MESSAGE_SIZE, stdin);

        // Mengisi pesan
        msg.mtype = 1;

        // Mengirim pesan ke message queue
        msgsnd(msgid, &msg, sizeof(msg), 0);

        if (strcmp(msg.mtext, "exit\n") == 0) {
            // Keluar dari loop jika pengguna memasukkan "exit"
            break;
        }

        if (strcmp(msg.mtext, "CREDS\n") == 0) {
            // Setelah mengirim CREDS, berikan prompt lagi
            continue;
        }

        if (strncmp(msg.mtext, "TRANSFER ", 9) == 0) {
            // Kirim perintah "TRANSFER" ke receiver
            continue;
        }

        // Menerima pesan balasan dari receiver
        msgrcv(msgid, &msg, sizeof(msg), 1, 0);

        // Memeriksa apakah pesan balasan adalah pesan keberhasilan
        if (strcmp(msg.mtext, "Authentication successful") == 0) {
            printf("Authentication successful\n");
        } else if (strcmp(msg.mtext, "Authentication failed") == 0) {
            printf("Authentication failed\n");
        } else {
            printf("Pesan dari receiver: %s\n", msg.mtext);
        }
    }
```

Terdapat loop yang akan melakukan pengiriman message ke file `receiver.c`. Disini loop akan berhenti apabila user memberikan input 'exit'. 

### Penjelasan Receiver
`receiver.c`
#### Decode
```
#include <openssl/bio.h>

void base64_decode(const char *input, char *output) {
    BIO *b64, *bio;
    int input_len = strlen(input);

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    bio = BIO_new(BIO_s_mem());
    BIO_push(b64, bio);

    BIO_write(bio, input, input_len);
    BIO_flush(bio);

    int output_len = BIO_read(b64, output, MAX_MESSAGE_SIZE);

    BIO_free_all(b64);

    output[output_len] = '\0';
}
```

Untuk melakukan decode password, ditambahkan library `openssl/bio.h` yang akan memberikan akses pada fungsi-fungsi decoding BIO. 

#### CREDS
```
int processCREDS() {
    FILE *file = fopen(USERS_FILE, "r");
    if (file) {
        char line[256];
        while (fgets(line, sizeof(line), file)) {
            char *username = strtok(line, ":");
            char *encoded_password = strtok(NULL, ":");

            if (encoded_password) {
                char decoded_password[MAX_MESSAGE_SIZE];
                base64_decode(encoded_password, decoded_password);
                printf("Username: %s, Decoded Password: %s\n", username, decoded_password);
            }
        }
        fclose(file);
    } else {
        printf("Failed to open 'users.txt'.\n");
        return 0;
    }
    return 1;
}

```

Untuk menjalankan process CREDS pada program receiver, perlu didapatkan pesan "CREDS" yang dikirim dari program sendernya. Apabila hal tersebut telah dilakukan, maka fungsi CREDS akan membuka file users.txt yang berada dalam direktori users, lalu melakukan decoding password menggunakan fungsi base64_decode yang telah dibahas diatas.

#### AUTH
```
int processAUTH(char *username, char *input_password) {
    FILE *file = fopen(USERS_FILE, "r");
    if (file) {
        char line[256];
        while (fgets(line, sizeof(line), file)) {
            char *fileUsername = strtok(line, ":");
            char *encoded_password = strtok(NULL, ":");

            if (fileUsername && encoded_password) {
                char decoded_password[MAX_MESSAGE_SIZE];
                base64_decode(encoded_password, decoded_password);

                if (strcmp(username, fileUsername) == 0 &&
                    strcmp(input_password, decoded_password) == 0) {
                    fclose(file);
                    return 1;
                }
            }
        }
        fclose(file);
    }
    return 0;
}

```

Process AUTH akan berjalan apabila user menginputkan AUTH: _username password_ pada program `sender.c`. Disini program akan membuka file users.txt dan akan mengkomparasikan dengan input yang telah dimasukkan setelah "AUTH:". Program akan melakukan decode pada password yang ada di users.txt dan nantinya akan digunakan untuk melakukan authentikasi.

#### TRANSFER
```
int processTRANSFER(char *filename) {
    char source[300];
    char destination[300];

    sprintf(source, "Sender/%s", senderFile);
    sprintf(destination, "Receiver/%s", senderFile);

    struct stat st;
    if (stat(destination, &st) == 0){
        printf("Found a duplicate file in 'Receiver' directory\n");
    }
    else{
        if (rename(source, destination) == 0){
            if (stat(destination, &st) == 0){
                printf("Ukuran file yang dipindah: %lld KB\n", (long long)st.st_size / 1024);}
            else{
                perror("Error mendapatkan ukuran file");
            }
        }
    }
}
```

Process TRANSFER akan berjalan apabila user menginputkan TRANSFER _filename_ pada program `sender.c`. Disini program akan membuka path menuju direktori Sender dan akan mengecek apakah file tersebut ada atau tidak. Jika program ada didalam direktori Sender, maka file tersebut akan dipindahkan ke direktori Receiver beserta konfirmasi ukuran file yang telah dipindahkan.

#### Fungsi Main
```
int main() {
    key_t key = MESSAGE_KEY;
    int msgid;
    struct message msg;

    msgid = msgget(key, 0666 | IPC_CREAT);

    while (1) {
        msgrcv(msgid, &msg, sizeof(msg), 1, 0);

        printf("Received Message: %s\n", msg.mtext);

        if (strcmp(msg.mtext, "CREDS\n") == 0) {
            printf("Received 'CREDS'. Decrypting passwords...\n");
            processCREDS();
        } else if (strncmp(msg.mtext, "AUTH: ", 6) == 0) {
            char *authData = msg.mtext + 6;
            char *authUsername = strtok(authData, " ");
            char *authPassword = strtok(NULL, " ");
            if (authUsername && authPassword) {
                printf("Received 'AUTH' request. Processing...\n");
                if (processAUTH(authUsername, authPassword)) {
                    printf("Authentication successful for user: %s\n", authUsername);
                    strcpy(msg.mtext, "Authentication successful");
                } else {
                    printf("Authentication failed for user: %s\n", authUsername);
                    strcpy(msg.mtext, "Authentication failed");
                    break;
                }
                msg.mtype = 1;
                msgsnd(msgid, &msg, sizeof(msg), 0);
            } else {
                printf("Malformed 'AUTH' request.\n");
            }
        } else if (strncmp(msg.mtext, "TRANSFER ", 8) == 0) {
            char *filename = msg.mtext + 8;
            if (filename) {
                if (processTRANSFER(filename)) {
                    strcpy(msg.mtext, "File transfer successful");
                } else {
                    strcpy(msg.mtext, "File transfer failed");
                    break;
                }
                msg.mtype = 1;
                msgsnd(msgid, &msg, sizeof(msg), 0);
            } else {
                printf("Malformed 'TRANSFER' request.\n");
            }
        } else {
            printf("UNKNOWN COMMAND\n");
        }
    }

    // Receiver akan terus berjalan dalam loop

    return 0;
}
```
Fungsi utama , program berperan sebagai server yang mendengarkan pesan pada antrean pesan dan melakukan tindakan seperti otentikasi dan transfer file berdasarkan pesan yang diterima. Ini memberikan umpan balik tentang status otentikasi dan keberhasilan atau kegagalan transfer file.

## Output Soal 3

## Revisi Soal 3
Revisi yang dilakukan ada pada bagian process AUTH yang sebelumnya hanya bisa successful dengan menggunakan password dari users.txt, sekarang dapat menggunakan password yang telah di decrypt.

Revisi lainnya yakni telah menambahkan fitur status autentikasi, dimana apabila belum melakukan autentikasi maka belum dapat melakukan proses transfer. Dilanjutkan lagi pada perbaikan proses TRANSFER yang sebelumnya belum bisa membaca direktori Sender, kini sudah berjalan dengan normal.

## Kendala Soal 3
Untuk melakukan authentikasi dengan password yang telah didecrypt terdapat beberapa kendala, yakni kurangnya pemahaman pada soal dan logic yang seharusnya ada pada fungsi AUTH tersebut.

# Soal 4

## Deskripsi Soal 4
Aplikasi yang memanfaatkan socket untuk menghubungkan client ke server, memungkinkan server untuk menerima dan menampilkan pesan dari client, dan memastikan bahwa server hanya dapat menerima koneksi dari 5 client sekaligus.

## Penyelesaian Soal 4

### Point A dan B

Client dan server terhubung melalui socket. Server berfungsi sebagai penerima pesan dari client dan hanya menampilkan pesan saja.  

#### Server.c
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>

#define MAX_CONNECTIONS 5

pthread_mutex_t mutex; // Mutex untuk mengamankan akses active_connection
int active_connections = 0; // Jumlah koneksi aktif

// Fungsi untuk menangani koneksi dengan client
void* handle_client(void* client_socket_ptr) {// menerima pointer client_socket_ptr yang mengacu pada socket klien
    int client_socket = ((int)client_socket_ptr);//pointer tersebut dikonversi ke tipe data int dan disimpan dalam variabel client_socket.
    free(client_socket_ptr);

    char pesan[1024];//untuk menerima pesan dari client

    while (1) {
        ssize_t bytes_received = recv(client_socket, pesan, sizeof(pesan), 0); //recv untuk menerima data dari socket klien
        if (bytes_received <= 0) {//Jika bytes_received kurang dari atau sama dengan 0, loop akan dihentikan.
            break;
        }
        pesan[bytes_received] = '\0';//null-terminator ditambahkan pada akhir pesan untuk membuatnya menjadi string yang valid
        printf("Pesan dari client %d: %s\n", client_socket, pesan);//pesan dicetak ke layar bersama dengan nomor klien 
    }

    printf("Client %d telah keluar.\n", client_socket);//Setelah loop selesai program mencetak pesan bahwa koneksi dengan klien telah ditutup, 
    close(client_socket);

    // Mengurangi jumlah koneksi aktif
    pthread_mutex_lock(&mutex);//mengunci mutex untuk mencegah adanya akses bersamaan ke variabel active_connections
    active_connections--;// koneksi dengan klien yang ditangani oleh thread ini telah selesai atau terputus, sehingga jumlah koneksi aktif berkurang.
    pthread_mutex_unlock(&mutex);//melepaskan (unlock) mutex agar memungkinkan thread lain untuk mengunci mutex dan mengakses bagian kritis secara aman.

    return NULL;
}

int main() {
    int server_socket, client_socket;//server_socket akan digunakan untuk mendengarkan koneksi dari klien, client_socket digunakan untuk menerima koneksi dari klien.
    struct sockaddr_in server_address, client_address;// server_address untuk mengatur alamat dan port server, client_address untuk menyimpan informasi alamat klien saat koneksi diterima.
    socklen_t client_len = sizeof(client_address);// menyimpan alamat client yang terhubung

    // Inisialisasi mutex
    if (pthread_mutex_init(&mutex, NULL) != 0) {
        perror("Error in pthread_mutex_init");
        exit(1);
    }

    // Inisialisasi socket
    server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket == -1) {//jika bytes_received kurang dari atau sama dengan 0, loop akan dihentikan
        perror("Error in socket");
        exit(1);
    }

    // Bind socket ke alamat dan port
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(23456);
    server_address.sin_addr.s_addr = INADDR_ANY;

    if (bind(server_socket, (struct sockaddr*)&server_address, sizeof(server_address)) == -1) {//mengikat socket server (server_socket) ke alamat dan port yang telah ditentukan 
        perror("Error in bind");
        exit(1);
    }

    // Listen hingga 5 koneksi client
    if (listen(server_socket, MAX_CONNECTIONS) == -1) {
        perror("Error in listen");
        exit(1);
    }

    printf("Server siap menerima koneksi dari %d client...\n", MAX_CONNECTIONS);

    while (1) {//menjalankan server secara terus-menerus, selama program berjalan.
        if (active_connections < MAX_CONNECTIONS) {//memeriksa apakah jumlah koneksi aktif kurang dari jumlah maksimum 
            //menerima koneksi dari klien menggunakan fungsi accept agar socket klien (client_socket) dapat berkomunikasi dengan klien .
            client_socket = accept(server_socket, (struct sockaddr*)&client_address, &client_len);
            if (client_socket == -1) {
                perror("Error in accept");
                exit(1);
            }
            printf("Terhubung dengan client %d\n", client_socket);

            // Membuat thread untuk menangani koneksi dengan client
            pthread_t client_thread;
            int* client_socket_ptr = malloc(sizeof(int));//mengirim informasi socket klien (client_socket) ke thread baru
            *client_socket_ptr = client_socket;
            if (pthread_create(&client_thread, NULL, handle_client, (void*)client_socket_ptr) != 0) {
                perror("Error in pthread_create");
                exit(1);
            }

            // Memungkinkan thread untuk berjalan secara independen
            pthread_detach(client_thread);

            // Menambah jumlah koneksi aktif
            pthread_mutex_lock(&mutex); //mengunci mutex (mutex) untuk mengamankan operasi penambahan.
            active_connections++;//menambahkan jumlah koneksi aktif dengan satu.
            pthread_mutex_unlock(&mutex);//membuka kunci mutex untuk memungkinkan thread lain untuk mengakses variabel active_connections
        } else {
            printf("Server telah mencapai batas maksimum koneksi aktif (%d).\n", MAX_CONNECTIONS);

            // Menunggu hingga ada koneksi yang terputus
            while (active_connections >= MAX_CONNECTIONS) {//menunggu hingga ada koneksi yang terputus sehingga ada ruang untuk menerima koneksi baru. 
                sleep(1);
            }
        }
    }

    // Menutup server socket
    close(server_socket);

    // Menghancurkan mutex
    pthread_mutex_destroy(&mutex);

    return 0;
}

```
Kode yang Anda berikan adalah implementasi server socket sederhana yang menerima koneksi dari beberapa client dan menangani pesan yang diterima dari client. Saya akan menjelaskan kode tersebut secara rinci:

1. **Inklusi Header**:
   - Kode dimulai dengan inklusi beberapa header file yang diperlukan, seperti `stdio.h`, `stdlib.h`, `string.h`, `unistd.h`, `arpa/inet.h`, dan `pthread.h`.

2. **Definisi Konstanta**:
   - Kode mendefinisikan konstanta `MAX_CONNECTIONS` yang menentukan jumlah maksimum koneksi client yang diizinkan pada server. Dalam hal ini, jumlahnya adalah 5.

3. **Deklarasi Mutex**:
   - Mutex (`pthread_mutex_t`) digunakan untuk mengamankan akses ke variabel `active_connections` yang digunakan untuk melacak jumlah koneksi aktif. Mutex digunakan untuk menghindari situasi perlombaan (race condition) ketika beberapa thread mencoba mengakses dan memodifikasi variabel ini.

4. **Variabel Jumlah Koneksi Aktif**:
   - Variabel `active_connections` digunakan untuk melacak jumlah koneksi client yang sedang aktif. Ini akan ditingkatkan setiap kali koneksi baru diterima dan akan dikurangi ketika koneksi tertentu ditutup.

5. **Fungsi `handle_client`**:
   - Fungsi ini akan dijalankan dalam thread terpisah untuk menangani komunikasi dengan setiap client yang terhubung. Ini mengambil argumen dalam bentuk pointer ke socket client, menerima pesan dari client, dan mencetak pesan tersebut ke layar.

6. **Fungsi `main`**:
   - Fungsi utama program dimulai. Ini melakukan langkah-langkah berikut:

   a. Inisialisasi Mutex:
      - Mutex diinisialisasi dengan `pthread_mutex_init` untuk mengamankan akses ke `active_connections`.

   b. Inisialisasi Socket Server:
      - Server socket diinisialisasi menggunakan `socket()` dan diikat ke alamat dan port yang ditentukan dengan menggunakan `bind()`.

   c. Listen untuk Koneksi:
      - Server mendengarkan koneksi client dengan memanggil `listen()`.

   d. Loop Utama:
      - Server berjalan dalam loop utama yang tak berakhir.
      - Jika jumlah koneksi aktif kurang dari maksimum yang diizinkan, maka server akan mencoba menerima koneksi baru dari client.

   e. Terima Koneksi Client:
      - Jika ada koneksi yang tersedia, server akan menerima koneksi menggunakan `accept()` dan mencetak pesan bahwa server terhubung dengan client tertentu.

   f. Buat Thread untuk Menangani Koneksi:
      - Untuk setiap koneksi client yang berhasil diterima, server membuat thread baru dengan menggunakan `pthread_create` dan menjalankan fungsi `handle_client` pada thread tersebut untuk menangani komunikasi dengan client.

   g. Detach Thread:
      - Thread yang dibuat dengan `pthread_create` di-detach agar dapat berjalan secara independen tanpa perlu menunggu thread tersebut untuk menyelesaikan eksekusi.

   h. Tambah Jumlah Koneksi Aktif:
      - Setelah menerima koneksi baru, jumlah koneksi aktif ditingkatkan dan mutex digunakan untuk mengamankan operasi penambahan ini.

   i. Periksa Batas Koneksi Maksimum:
      - Jika jumlah koneksi aktif telah mencapai batas maksimum yang diizinkan, server akan mencetak pesan bahwa server telah mencapai batas maksimum koneksi aktif.

   j. Tunggu Hingga Koneksi Terputus:
      - Server akan memasuki loop yang mengulang hingga ada koneksi yang terputus (sehingga ada ruang untuk menerima koneksi baru). Hal ini dilakukan dengan tidur selama 1 detik (`sleep(1)`).

   k. Tutup Server Socket:
      - Setelah program selesai, server socket ditutup dengan `close(server_socket)`.

   l. Hancurkan Mutex:
      - Mutex dihancurkan dengan `pthread_mutex_destroy`.

7. **Penutupan Koneksi**:
   - Dalam fungsi `handle_client`, koneksi client ditutup setelah selesai mengirim dan menerima pesan dari client.

Ini adalah implementasi dasar dari server socket yang dapat menerima hingga 5 koneksi client secara bersamaan dan menangani pesan yang diterima dari masing-masing client. Pada dasarnya, server ini berjalan terus-menerus dan memungkinkan multiple client untuk terhubung dan berkomunikasi dengan server secara bersamaan.

#### Client.c
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

int main() {
    int client_socket;//untuk berkomunikasi dengan klien tertentu setelah koneksi diterima oleh server.
    struct sockaddr_in server_address;//mengatur alamat dan port server

    // Inisialisasi socket
    client_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (client_socket == -1) {
        perror("Error in socket");
        exit(1);
    }

    // Konfigurasi alamat server
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(23456);
    server_address.sin_addr.s_addr = inet_addr("127.0.0.1");

    //membuat koneksi ke alamat yang ada dalam server_address melalui client_socket
    if (connect(client_socket, (struct sockaddr*)&server_address, sizeof(server_address)) == -1) {
        perror("Error in connect");
        exit(1);
    }


    char pesan[1024];//menyimpan pesan yang akan dikirim ke server.

    while (1) {//melakukan input dan mengirimkannya ke server.
        printf("Masukkan pesan untuk server (ketik 'exit' untuk keluar): ");
        fgets(pesan, sizeof(pesan), stdin);

        send(client_socket, pesan, strlen(pesan), 0);//mengirim pesan yang telah dimasukkan oleh pengguna ke server melalui koneksi (client_socket)

        if (strcmp(pesan, "exit\n") == 0) {
            // Keluar dari loop jika pengguna memasukkan 'exit'
            break;
        }
    }

    // Menutup koneksi client
    close(client_socket);

    return 0;
}
```

1. **Header dan Library**: Kode dimulai dengan beberapa baris yang mengimpor header dan library yang diperlukan. Beberapa di antaranya adalah `stdio.h`, `stdlib.h`, `string.h`, `unistd.h`, dan `arpa/inet.h`. Ini adalah library yang digunakan untuk operasi input/output, alokasi memori, dan operasi socket.

2. **Deklarasi Variabel**: Dalam kode ini, ada beberapa variabel yang dideklarasikan:

    - `client_socket`: Variabel ini akan digunakan untuk berkomunikasi dengan server setelah koneksi diterima oleh klien.
    
    - `server_address`: Struktur ini digunakan untuk mengatur alamat dan port server yang akan dikoneksikan oleh klien.

3. **Inisialisasi Socket**: Kode memulai dengan inisialisasi socket klien menggunakan `socket` dengan menggunakan protokol `AF_INET` dan tipe `SOCK_STREAM` (untuk TCP). Jika inisialisasi gagal, pesan kesalahan akan dicetak.

4. **Konfigurasi Alamat Server**: Alamat dan port server yang akan dihubungi oleh klien dikonfigurasi dalam variabel `server_address`. Dalam contoh ini, server berjalan di alamat IP `127.0.0.1` (localhost) dan port `23456`.

5. **Membuat Koneksi ke Server**: Klien membuat koneksi ke server menggunakan fungsi `connect`. Fungsi ini akan mencoba menghubungkan klien ke alamat server yang telah dikonfigurasi dalam `server_address`. Jika koneksi gagal, pesan kesalahan akan dicetak.

6. **Loop Utama**: Setelah koneksi berhasil dibuat, klien memasuki loop utama yang akan berjalan terus-menerus.

    - Klien meminta pengguna untuk memasukkan pesan dengan mencetak "Masukkan pesan untuk server (ketik 'exit' untuk keluar): ".
    
    - Pengguna memasukkan pesan melalui `fgets` dan pesan tersebut disimpan dalam variabel `pesan`.

    - Pesan dikirim ke server menggunakan fungsi `send`. Fungsi ini mengirim pesan yang telah dimasukkan oleh pengguna ke server melalui koneksi yang telah dibuat sebelumnya (`client_socket`).

    - Jika pengguna memasukkan "exit\n", klien akan keluar dari loop utama.

7. **Menutup Koneksi Klien**: Setelah klien keluar dari loop utama (biasanya setelah pengguna memasukkan "exit"), koneksi klien ditutup dengan menggunakan `close(client_socket)`.


## Output Soal 4
**Menjalankan server**
![Menjalankan server](https://i.imgur.com/mCwIKHR.png)

**Menjalankan dan mengirim pesan client 1-5**
![Menjalankan client](https://i.imgur.com/wTAzIgX.png)

**Tampilan server ketika terhubung dan menerima pesan dari client**
![Tampilan server ketika 5 client](https://i.imgur.com/KUJmp1C.png)

**Menjalankan client 6**
![Menjalankan client 6](https://i.imgur.com/kK7fhdH.png)

**Tampilan server ketika terhubung dan menerima pesan dari client**
![Tampilan server](https://i.imgur.com/PBMkTHk.png)

**Melakukan terminate pada client 5**
![Terminate pada client 5](https://i.imgur.com/UoRPBkJ.png)

**Tampilan server ketika client 6 bisa terhubung ketika client 5 di terminate**
![Tampilan server](https://i.imgur.com/cnnM8wR.png)

## Revisi Soal 4
Tidak ada

## Kendala Soal 4
Tidak ada
